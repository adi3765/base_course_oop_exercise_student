package AerialVehicles;

import Entities.Coordinates;
import Enums.FlightStatus;
import Enums.MissionType;
import Outputs.ConsoleWriter;

import java.util.List;

public abstract class AerialVehicle {
    private int flightHoursLastVersion;
    private FlightStatus flightStatus;
    private Coordinates baseAirplaneLocation;
    private int maxFixHours;
    private List<MissionType> missions;

    public AerialVehicle(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        this.flightHoursLastVersion = flightHoursLastVersion;
        this.flightStatus = flightStatus;
        this.baseAirplaneLocation = baseAirplaneLocation;
    }

    public void setMissions(List<MissionType> missions) {
        this.missions = missions;
    }

    public List<MissionType> getMissions() {
        return missions;
    }

    public int getMaxFixHours() {
        return maxFixHours;
    }

    public void setMaxFixHours(int maxFixHours) {
        this.maxFixHours = maxFixHours;
    }

    public Coordinates getBaseAirplaneLocation() {
        return baseAirplaneLocation;
    }

    public void setFlightHoursLastVersion(int flightHoursLastVersion) {
        this.flightHoursLastVersion = flightHoursLastVersion;
    }

    public int getFlightHoursLastVersion() {
        return flightHoursLastVersion;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void flyTo(Coordinates destination) {
        if (this.flightStatus == FlightStatus.READY) {
            ConsoleWriter.write("Fly to: " + destination.toString());
            this.setFlightStatus(FlightStatus.ON_AIR);
        } else if(this.flightStatus == FlightStatus.NOT_READY) {
            ConsoleWriter.write("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination) {
        ConsoleWriter.write("Landing on: " + destination.toString());
        this.check();
    }

    public void check() {
        if (this.getFlightHoursLastVersion() >= maxFixHours) {
            this.setFlightStatus(FlightStatus.NOT_READY);
            this.repair();
        } else {
            this.setFlightStatus(FlightStatus.READY);
        }
    }

    public void repair() {
        this.setFlightHoursLastVersion(0);
        this.setFlightStatus(FlightStatus.READY);
    }
}
