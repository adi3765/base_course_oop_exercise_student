package AerialVehicles.Airplanes;

import AerialVehicles.AerialVehicle;
import Enums.FlightStatus;
import Entities.Coordinates;

public abstract class Airplane extends AerialVehicle {
    private final int MAX_FIX_HOURS = 250;

    public Airplane(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        super(flightHoursLastVersion, flightStatus, baseAirplaneLocation);
        this.setMaxFixHours(MAX_FIX_HOURS);
    }
}
