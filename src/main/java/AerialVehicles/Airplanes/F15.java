package AerialVehicles.Airplanes;


import Enums.FlightStatus;
import Entities.Coordinates;
import Enums.MissionType;

import java.util.ArrayList;
import java.util.List;

public class F15 extends Airplane {
    public F15(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        super(flightHoursLastVersion, flightStatus, baseAirplaneLocation);
        List<MissionType> missions = new ArrayList<MissionType>();
        missions.add(MissionType.ATTACK);
        missions.add(MissionType.INTELLIGENCE);
        this.setMissions(missions);
    }
}
