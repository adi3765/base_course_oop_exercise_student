package AerialVehicles.Remote;

import AerialVehicles.AerialVehicle;
import Enums.FlightStatus;
import Entities.Coordinates;
import Outputs.ConsoleWriter;

public abstract class RemotePlane extends AerialVehicle {
    public RemotePlane(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        super(flightHoursLastVersion, flightStatus, baseAirplaneLocation);
    }

    public String hoverOverLocation(Coordinates destination) {
        String coordinatesValues = "Hovering Over: " + destination.toString();

        this.setFlightStatus(FlightStatus.ON_AIR);
        ConsoleWriter.write(coordinatesValues);

        return coordinatesValues;
    }
}
