package AerialVehicles.Remote;


import Enums.FlightStatus;
import Entities.Coordinates;
import Enums.MissionType;

import java.util.ArrayList;
import java.util.List;

public class Zik extends Hermes {
    public Zik(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        super(flightHoursLastVersion, flightStatus, baseAirplaneLocation);

        List<MissionType> missions = new ArrayList<MissionType>();
        missions.add(MissionType.INTELLIGENCE);
        missions.add(MissionType.BDA);
        this.setMissions(missions);
    }
}