package AerialVehicles.Remote;

import Enums.FlightStatus;
import Entities.Coordinates;
import Enums.MissionType;

import java.util.ArrayList;
import java.util.List;

public class Kochav extends Hermes {
    public Kochav(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        super(flightHoursLastVersion, flightStatus, baseAirplaneLocation);

        List<MissionType> missions = new ArrayList<MissionType>();
        missions.add(MissionType.ATTACK);
        missions.add(MissionType.INTELLIGENCE);
        missions.add(MissionType.BDA);
        this.setMissions(missions);
    }
}
