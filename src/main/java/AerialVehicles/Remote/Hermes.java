package AerialVehicles.Remote;

import Enums.FlightStatus;
import Entities.Coordinates;

public abstract class Hermes extends RemotePlane {
    private final int MAX_FIX_HOURS = 100;

    public Hermes(int flightHoursLastVersion, FlightStatus flightStatus, Coordinates baseAirplaneLocation) {
        super(flightHoursLastVersion, flightStatus, baseAirplaneLocation);
        this.setMaxFixHours(MAX_FIX_HOURS);
    }
}
