package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.MissionType;
import Enums.SensorType;
import Exceptions.AerialVehicleNotCompatibleException;

public class IntelligenceMission extends Mission {
    private String region;
    private SensorType sensorType;

    public IntelligenceMission(Coordinates destination, String pilotName,
                               AerialVehicle excecuteMission, String region, SensorType sensorType)
            throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, excecuteMission, MissionType.INTELLIGENCE);
        this.region = region;
        this.sensorType = sensorType;
    }

    @Override
    public String executeMission() {
        String vehicleType = this.getExcecuteVehicle().getClass().getName().substring(this.getExcecuteVehicle()
                .getClass().getName().lastIndexOf('.') + 1);

        return (this.getPilotName() + ": " + vehicleType + " Collecting Data in " + this.region +
                " with: sensor type " + this.sensorType);
    }
}
