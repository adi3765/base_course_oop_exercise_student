package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.CameraType;
import Enums.MissionType;
import Exceptions.AerialVehicleNotCompatibleException;

public class BdaMission extends Mission {
    private String objective;
    private CameraType cameraType;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle excecuteMission, String objective, CameraType cameraType)
            throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, excecuteMission, MissionType.BDA);
        this.objective = objective;
        this.cameraType = cameraType;
    }

    @Override
    public String executeMission() {
        String vehicleType = this.getExcecuteVehicle().getClass().getName().substring(this.getExcecuteVehicle()
                .getClass().getName().lastIndexOf('.') + 1);

        return (this.getPilotName() + ": " + vehicleType + " taking pictures of suspec " + this.objective + " with: " + cameraType);
    }
}
