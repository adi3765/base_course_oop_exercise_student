package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.MissileType;
import Enums.MissionType;
import Exceptions.AerialVehicleNotCompatibleException;

public class AttackMission extends Mission {
    private String target;
    private int missileAmount;
    private MissileType missileType;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle excecuteMission,
                         String target, int missileAmount, MissileType missileType) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, excecuteMission, MissionType.ATTACK);
        this.target = target;
        this.missileAmount = missileAmount;
        this.missileType = missileType;
    }

    @Override
    public String executeMission() {
        String vehicleType = this.getExcecuteVehicle().getClass().getName().substring(this.getExcecuteVehicle()
                .getClass().getName().lastIndexOf('.') + 1);

        return (this.getPilotName() + ": " + vehicleType + " Attacking suspect " + this.target + " with: " +
                missileType + "X" + this.missileAmount);
    }
}