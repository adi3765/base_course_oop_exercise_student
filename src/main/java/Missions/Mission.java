package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.MissionType;
import Exceptions.AerialVehicleNotCompatibleException;
import Outputs.ConsoleWriter;

public abstract class Mission implements MissionActions {
    private Coordinates destination;
    private String pilotName;
    private AerialVehicle excecuteVehicle;
    private MissionType missionType;

    public Mission(Coordinates destination, String pilotName, AerialVehicle excecuteVehicle, MissionType missileType)
            throws AerialVehicleNotCompatibleException {
        this.destination = destination;
        this.pilotName = pilotName;
        this.excecuteVehicle = excecuteVehicle;
        this.missionType = missileType;
        this.checkMissionTypeValidation();
    }

    public void checkMissionTypeValidation() throws AerialVehicleNotCompatibleException {
        if (!this.excecuteVehicle.getMissions().contains(this.missionType)) {
            throw new AerialVehicleNotCompatibleException("This aerial vehicle doesn't fit the mission type");
        }
    }

    public AerialVehicle getExcecuteVehicle() {
        return excecuteVehicle;
    }

    public String getPilotName() {
        return pilotName;
    }

    @Override
    public void begin() {
        ConsoleWriter.write("Beginning Mission!");
        this.excecuteVehicle.flyTo(this.destination);
    }

    @Override
    public void cancel() {
        ConsoleWriter.write("Abort Mission!");
        this.excecuteVehicle.land(this.excecuteVehicle.getBaseAirplaneLocation());
    }

    @Override
    public void finish() {
        ConsoleWriter.write(this.executeMission());
        this.excecuteVehicle.land(this.destination);
        ConsoleWriter.write("Finish Mission!");
    }

    public abstract String executeMission();
}
