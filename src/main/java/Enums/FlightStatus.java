package Enums;

public enum FlightStatus {
    READY,
    NOT_READY,
    ON_AIR
}
