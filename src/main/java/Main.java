import AerialVehicles.Airplanes.F15;
import Enums.FlightStatus;
import AerialVehicles.Remote.Kochav;
import Entities.Coordinates;
import Enums.MissileType;
import Exceptions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Enums.CameraType;
import Outputs.ConsoleWriter;

public class Main {
    public static void main(String[] args) {
        F15 baz = new F15(15, FlightStatus.ON_AIR, new Coordinates(1.1, 5.5));
        Kochav kochav = new Kochav(6, FlightStatus.READY, new Coordinates(4.6, 10.2));

        try {
            // Those examples should work
            BdaMission mission = new BdaMission(new Coordinates(3.3, 2.1), "Adi", kochav,
                    "out", CameraType.REGULAR);
            AttackMission attackMission = new AttackMission(new Coordinates(6.6, 5.4), "Adi", baz,
                    "out", 16, MissileType.AMRAM);
            mission.begin();
            attackMission.begin();
            attackMission.finish();

            // This example should throw an error- bda mission to f15- doesn't fit
            BdaMission bdaMission = new BdaMission(new Coordinates(3.3, 2.1), "Adi", baz,
                    "out", CameraType.REGULAR);

        } catch (AerialVehicleNotCompatibleException error) {
            ConsoleWriter.write(error.getMessage());
        }
    }
}
